# midy
Visual Programming Final Project (Fall 2011)

## Description
midy is subtitles application for instant news flow. Application offers details of twitter, breaking news, facebook, e-mail, exchange, alarm and activities every 10 minutes.

<img src="https://gitlab.com/kyellow/midy/raw/old/screenshot/midy1.png" width="687" height="100"/>

## Relational Database schema
<img src="https://gitlab.com/kyellow/midy/raw/old/screenshot/database.png" width="400" height="200"/>

## * [Midy Project Old Codes Made During School (Branch -> old)](https://gitlab.com/kyellow/midy/blob/old/README.md)
## * [Then Midy Project Was Recoded For Firefox Browser (Branch -> browser)](https://gitlab.com/kyellow/midy/blob/master/README.md)
